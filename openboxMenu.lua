--[[
--
-- Author: OndraK
--
-- This piece of lua code can be distributed under the terms of GNU GPL v3
--
--]]

local openboxMenu = {}

-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- private functions -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function beginPipemenu()
	print([[<?xml version="1.0" encoding="UTF-8"?>
<openbox_pipe_menu>]])
end

local function endPipemenu()
	print('</openbox_pipe_menu>')
end

local function escapeAttribute(str)
	local escaped = '[ ,]'
	return str:lower():gsub(escaped, "-"):gsub("-+", "-")
end

local function escapeHtmlEntities(text)
	local escaped = '["&<>]'
	local entities = {
		["&"] = "&amp;",
		['"'] = "&quot;",
		["<"] = "&lt;",
		[">"] = "&gt;"
	}
	return text:gsub(escaped, entities)
end

local function escapeTable(tbl)
	local escaped = {}
	for i, item in pairs(tbl) do
		escaped[i] = type(item) == "table" and escapeTable(item) or escapeHtmlEntities(item)
	end
	return escaped
end

local function escapedParameters(func)
	return function(...)
		return func(unpack(escapeTable({...})))
	end
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- public functions  -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

openboxMenu.button = decorator(escapedParameters) ..
function(text, commands, icon)
	assert(text, "Text of button not specified.")
	assert(commands, "Button command not specified.")
	icon = icon or ""
	commands = type(commands) == "string" and { commands } or commands
	print(string.format('<item label="%s" icon="%s">', text, icon))
	for _, command in ipairs(commands) do
		print('<action name="execute">')
		print(string.format('<command>%s</command>', command))
		print('</action>')
	end
	print('</item>')
end

openboxMenu.item = decorator(escapedParameters) ..
function(text, icon)
	assert(text, "Text of menu item not specified.")
	icon = icon or ""
	print(string.format('<item label="%s" icon="%s" />', text, icon))
end

function openboxMenu.separator()
	print('<separator/>')
end

openboxMenu.title = decorator(escapedParameters) ..
function(title)
	assert(title, "Separator label not specified")
	print(string.format('<separator label="%s" />', title))
end

openboxMenu.beginMenu = decorator(escapedParameters) ..
function(id, label)
	assert(id, "Menu id not specified")
	assert(label, "Menu label not specified")
	print(string.format('<menu id="%s" label="%s">', escapeAttribute(id), label))
end

function openboxMenu.endMenu()
	print('</menu>')
end

openboxMenu.subPipemenu = decorator(escapedParameters) ..
function(id, label, execute, icon)
	assert(id, "Pipemenu id not specified")
	assert(label, "Pipemenu label not specified")
	assert(execute, "Pipemenu command not specified")
	icon = icon or ""
	print(string.format('<menu id="%s" label="%s" execute="%s" icon="%s" />', id, label, execute, icon))
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- decorators  -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

function openboxMenu.pipemenu(title)
	return function(func)
		return function(...)
			beginPipemenu()
			if title then
				openboxMenu.title(title)
			end
			func(unpack{...})
			endPipemenu()
		end
	end
end

return openboxMenu

