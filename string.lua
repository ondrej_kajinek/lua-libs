--[[
--
-- Author: OndraK
--
-- This piece of lua code can be distributed under the terms of GNU GPL v3
--
--]]

-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- public functions  -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

function string.bashEscape(str)
	local toEscape = "[ ;&()'\"]"
	local escaped = str:gsub(toEscape, createEscapings(toEscape))
	return escaped
end
