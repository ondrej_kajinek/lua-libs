--[[
--
-- Author: OndraK
--
-- This piece of lua code can be distributed under the terms of GNU GPL v3
--
--]]

local mpd = {}

local selfDir = debug.getinfo(1).source:gsub("@", ""):gsub("[^/]+$", "")

local system = require "libs/system"
require "libs/string"
require "libs/decorators"
require "libs/DirStructure"


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- private functions -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local cmds = {
	currentSong = "mpc -f '%album% - %title%' current",
	currentSongFile = 'mpc current -f %file%',
	optionTemplate = system.pipe("mpc status", "grep -o '%s: on'"),
	pathTemplate = system.pipe("grep '^%s' /etc/mpd.conf", "grep -oP '/[/\\w]+'"),
	playlistTemplate = "mpc -f '%s' playlist"
}

local discographyNames = {
	["diskografie"] = true
}

local filters = {
	playlist = "grep '\\.m3u$'"
}

local albumartSuffices = {
	"jpg", "jpeg"
}

local function getMusicLibraryDir()
	return system.singleResult(string.format(cmds.pathTemplate, 'music_directory'))
end

local function mpcResult(cmd)
	return system.singleResult(cmd)
end

local separators = {
	playlist = "::",
	tag = "::C++::"
}

local tags = {
	"%album%", "%track%", "%title%"
}

local function newAlbumNode(name)
	return {
		name = name,
		tracks = {}
	}
end

local function newPlaylistNode(name)
	return {
		name = name,
		playlists = {}
	}
end

local function newTrackNode(trackNumber, trackName, playlistNumber)
	return {
		name = string.format("%02d - %s", trackNumber or 0, trackName),
		number = playlistNumber
	}
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- public functions  -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

mpd.availableAlbumarts = decorator(coroutine_wrap) ..
function()
	local currentDir = system.parentDir(system.path(getMusicLibraryDir(), mpcResult(cmds.currentSongFile)))
	local ls = string.format('ls %s', currentDir:bashEscape())
	local imageFilter = string.format("grep -E '%s'", table.concat(albumartSuffices, '|'))
	for image in system.resultLines(system.pipe(ls, imageFilter)) do
		coroutine.yield(currentDir, image)
	end
end

function mpd.currentPlaylist()
	local albums = {}
	local albumIndices = {}
	local trackNo = 1
	for track in mpd.playlist(tags) do
		local albumName, trackNumber, trackName = unpack(track:split(separators.tag, 2))
		local trackNumber = trackNumber:match("%d+")
		if not albumIndices[albumName] then
			albumIndices[albumName] = #albums + 1
			albums[#albums + 1] = newAlbumNode(albumName)
		end
		local albumIndex = albumIndices[albumName]
		table.insert(albums[albumIndex].tracks, newTrackNode(trackNumber, trackName, trackNo))
		trackNo = trackNo + 1
	end
	return albums
end

function mpd.currentSong()
	return system.singleResult(cmds.currentSong)
end

function mpd.currentSongDir()
	return system.parentDir(system.path(getMusicLibraryDir(), mpcResult(cmds.currentSongFile)))
end

function mpd.currentSongPath()
	return mpcResult(mpc.currentSongFile)
end

function mpd.fullPlaylistName(playlist, parent)
	return parent and (parent .. separators.playlist .. playlist) or playlist
end

function mpd.option(option)
	return system.singleResult(string.format(cmds.optionTemplate, option)) and "on" or "off"
end

function mpd.path(path)
	return system.singleResult(string.format(cmds.pathTemplate, path))
end

function mpd.playlist(tags)
	local playlistCmd = string.format(cmds.playlistTemplate, table.concat(tags, separators.tag))
	return system.resultLines(playlistCmd)
end

function mpd.playlists()
	local playlistDir = mpd.path("playlist_directory")
	local lsCmd = system.pipe(string.format("ls %s", playlistDir), filters.playlist)
	return system.resultLines(lsCmd)
end

function mpd.savedPlaylists()
	local playlistTree = DirStructure()
	for playlist in mpd.playlists() do
		local playlistName = system.stripSuffix(playlist)
		playlistTree.addFile(playlistName, separators.playlist)
	end
	return playlistTree.getDir()
end

return mpd

