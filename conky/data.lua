--[[
--
-- Author: OndraK
--
-- This piece of code can be redistributed under the terms of GNU GPL-3
--
--]]


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- --  PRIVATE -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

require "libs/decorators"


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- --  PUBLIC  -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function rangedValue(range)
	return function(dataSource)
		return function(...)
			local value = tonumber(dataSource{...}) or range.min
			return range.min + (value - range.min)%(range.max - range.min)
		end
	end
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- -- data  -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --
return {
	rangedValue = rangedValue,
}

