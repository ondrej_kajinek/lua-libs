--[[
--
-- Author: OndraK
--
-- This piece of code can be redistributed under the terms of GNU GPL-3
--
--]]


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- --  PUBLIC  -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function background(source)
	return {
		source = source
	}
end

local function border(width, colour)
	return {
		width = width,
		colour = colour
	}
end

local function center(x, y)
	return {
		x = x,
		y = y
	}
end

local function range(min, max)
	return {
		min = min,
		max = max + 1		-- from min inclusive to max exclusive
	}
end

local function radius(inner, margin)
	return {
		inner = inner,
		margin = margin
	}
end

local function rotation(start, range)
	return {
		start = start,
		range = range
	}
end

local function rgba(red, green, blue, alpha)
	return { red/255, green/255, blue/255, alpha }
end

return {
	background = background,
	border = border,
	center = center,
	radius = radius,
	range = range,
	rotation = rotation,
	rgba = rgba
}

