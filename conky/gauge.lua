--[[
--
-- Author: OndraK
--
-- This piece of code can be redistributed under the terms of GNU GPL-3
--
--]]


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- --  PRIVATE -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local cairo = require "cairo"
local pi, sin, cos = math.pi, math.sin, math.cos

local cairo_font_weight = CAIRO_FONT_WEIGHT_NORMAL
local cairo_line_cap = CAIRO_LINE_CAP_BUTT


local function set_source_rgba(display, colour)
	cairo_set_source_rgba(display, colour[1], colour[2], colour[3], colour[4])
end

local function draw_marks(display, ring, radius, marks)
	radius = radius - marks.size.length/2

	cairo_set_line_width(display, marks.size.width)
	set_source_rgba(display, marks.colour)
	for mark = 1, marks.count do
		local angle = ring.rotation.start + (mark - 1)/(marks.count - 1)*ring.rotation.range
		local start_x = ring.center.x + radius*cos(angle)
		local start_y = ring.center.y + radius*sin(angle)
		local stop_x = start_x + marks.size.length*cos(angle)
		local stop_y = start_y + marks.size.length*sin(angle)
		cairo_move_to(display, start_x, start_y)
		cairo_line_to(display, stop_x, stop_y)
		cairo_stroke(display)
	end
end

local function draw_ring(display, ring, radius)
	local angle_start, angle_stop = ring.rotation.start, ring.rotation.range + ring.rotation.start
	local bg_colour = ring.border.colour
	cairo_set_line_width(display, ring.border.width)
	set_source_rgba(display, ring.border.colour)
	cairo_arc(display, ring.center.x, ring.center.y, radius, angle_start, angle_stop)
	cairo_stroke(display)
end

-- not revised
local function draw_data(display, ring, data, order)
	local angle_start, angle_stop = ring.rotation.start, ring.rotation.range + ring.rotation.start
	local radius = ring.radius.inner + (order - 1)*(ring.border.width + ring.radius.margin)
	draw_ring(display, ring, radius)
	local value_range = data.value_range
	local min, max = value_range.min, value_range.max
	local value = data.value()
	if value > min then
		local hand_colour = data.hand.colour
		local value_angle = angle_start + (value - min)*ring.rotation.range/(max - min)
		cairo_set_line_width(display, ring.border.width)
		cairo_set_source_rgba(display, unpack(data.hand.colour))
		cairo_arc(display, ring.center.x, ring.center.y, radius, angle_start, value_angle)
		cairo_stroke(display)
	end

	if data.marks then
		draw_marks(display, ring, radius, data.marks)
	end

	if data.label then
		local label = data.label
		local offset = label.offset
		local x = ring.center.x + offset.x
		local y = ring.center.y + offset.y
		local font_colour = label.font_colour
		cairo_set_source_rgba(display, unpack(label.font_colour))
		cairo_select_font_face(display, label.font, label.font_slant or CAIRO_FONT_SLANT_NORMAL, label.font_weight or cairo_font_weight)
		cairo_set_font_size(display, label.font_size)
		cairo_move_to(display, x, y)
		cairo_show_text(display, label.text)
		cairo_stroke(display)
	end
end

local function draw_gauge(display, gauge)
	if not (gauge.data and gauge.ring) then
		return
	end
	for order, data in pairs(gauge.data) do
		draw_data(display, gauge.ring, data, order)
	end
end

local function draw_gauges(display, gauges)
	for _, gauge in pairs(gauges) do
		draw_gauge(display, gauge)
	end
end

local function initialize_cairo(display)
	cairo_set_line_cap(display, cairo_line_cap)
end

-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- --  PUBLIC  -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function draw(gauges, display)
	initialize_cairo(display)
	draw_gauges(display, gauges)
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- -- -- gauge -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --
return {
	draw = draw
}

