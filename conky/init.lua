--[[
--
-- Author: OndraK
--
-- This piece of code can be redistributed under the terms of GNU GPL-3
--
--]]

--[[
--
-- This library should not be included by conky lua scripts, but by conky configuration through lua_load
--
--]]

package.path = "/home/ondrak/programming/lua/libs/?.lua;" .. package.path

-- No need to return, all non-local variables are visible in conky lua scripts
conky = {
	common = require "conky/common",
	data = require "conky/data",
	gauge = require "conky/gauge"
}

