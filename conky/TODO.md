# Graph
- Data
- Ring

## Data
- Hand
- Label (optional)
- Marks (optional)
- value: function returning value from range <0, 1)

## Ring
- Background (optional)
- Border
- Center
- Radius
- Rotation
- separated: boolean determining whether several data sets are drawn in their own rings, or into a single one

### Background
- Center
- source: image path

### Border
- Colour
- width: width of the line in pixels

### Center
- x, y: coordinates

### Hand
- Colour
- Size (optional): if defined, clock hand is drawn, otherwise highlighted border is used

### Label (deprecated)

### Marks
- Size
- count: how many marks to draw
- Colour

### Radius
- inner: radius of the inner ring
- margin (optional): margin between neighbouring rings, if rings are separated

### Rotation
- range: angle of the ring in radians
- rotation: rotation of the zero-value end, in radians
- clockwise: boolean

#### Colour
- quadruplet of red, green, blue and alpha

#### Size
- length: 
- width

