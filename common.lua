--[[
--
-- Author: OndraK
--
-- This piece of lua code can be distributed under the terms of GNU GPL v3
--
--]]

local function escapeLuaMagic(str)
	local escaped = str:gsub("([%^%$%(%)%%%.%[%]%*%+%-%?])", "%%%1")
	return escaped
end

function os.cmd_results(cmd)
	local f = assert(io.popen(cmd, "r"))
	local s = assert(f:read("*a"))
	f:close()
	s = s:gsub("^%s+", ""):gsub("%s+$", "")
	return s:gmatch("[^\n\r]+")
end

function string.split(str, separator, maxSplits)
	assert(str and separator, "Not enough arguments for string.split")
	assert(separator:len() > 0, "No separator given for string.split")
	maxSplits = maxSplits or math.huge

	str = str .. separator
	local partIterator = str:gmatch("(.-)" .. escapeLuaMagic(separator))
	local part = partIterator()
	local parts = {}
	while maxSplits > 0 and part do
		parts[#parts + 1] = part
		maxSplits = maxSplits - 1
		part = partIterator()
	end
	local tail = part
	for tailPart in partIterator do
		tail = tail .. separator .. tailPart
	end
	parts[#parts + 1] = tail
	return parts
end

function table.ensure(variable)
	return type(variable) == "table" and variable or { variable }
end

function table.filter(tbl, fction)
	filtered = {}
	for key, value in pairs(tbl) do
		if fction(value) then
			filtered[key] = value
		end
	end
	return filtered
end

function table.map(tbl, fction)
	for key, value in pairs(tbl) do
		tbl[key] = fction(value)
	end
	return tbl
end

function table.merge(first, second)
	local merged = {}
	for key, value in pairs(first) do
		merged[key] = value
	end
	for key, value in pairs(second) do
		merged[key] = value
	end
	return merged
end

function systemLanguage()
	return os.getenv("LANG"):match("^(%w+)_(%w+)%.([%w-]+)$") or "en"
end

