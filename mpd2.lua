--[[
--
-- Author: OndraK
--
-- This piece of lua code can be distributed under the terms of GNU GPL v3
--
]]--

local mpd = {}

local selfDir = debug.getinfo(1).source:gsub("@", ""):gsub("[^/]+$", "")

local system = require "libs/system"
require "libs/decorators"
require "libs/DirStructure"


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- decorators  -- -- -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function coroutine_wrap(func)
	return coroutine.wrap(func)
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- private properties	-- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local separators = {
	track = ':<>:'
}

local trackFields = {
	'position = %position%',
	'disc = %disc%',
	'album = %album%',
	'track = %track%'
}

-- TODO: replace trackFields with position = %position%, disc = %disc%, ..., a bit easier parsing
local commands = {
	currentPlaylist = string.format('mpc playlist -f "%s"', table.concat(trackFields, separators.track)),
	currentSongFile = 'mpc current -f %file%',
	listPlaylists = 'mpc lsplaylists',
	pathTemplate = system.pipe("grep '^%s' /etc/mpd.conf", "grep -oP '/[/\\w]+'"),
	status = 'mpc status'
}


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- private functions -- -- --
-- -- -- -- -- -- -- -- -- -- -- --

local function currentSongFile()
	return system.singleResult(commands.currentSongFile)
end

local function getMusicLibraryDir()
	return system.singleResult(string.format(commands.pathTemplate, 'music_directory'))
end

local function parseTrack(track)
	track = track .. separators.track
	local parsed = {}
	local iterator = track:gmatch('(.-)' .. separators.track)
	local part = iterator()
	while part do
		local field, value = part:match('(.+) = (.*)')
		parsed[field] = value
		part = iterator()
	end
	return parsed
end


-- -- -- -- -- -- -- -- -- -- -- --
-- -- -- public functions	-- -- --
-- -- -- -- -- -- -- -- -- -- -- --


mpd.currentPlaylist = decorator(coroutine_wrap) ..
function()
	for rawTrack in system.resultLines(commands.currentPlaylist) do
		coroutine.yield(parseTrack(rawTrack))
	end
end

mpd.savedPlaylists = function()
	return system.resultLines(system.pipe(commands.listPlaylists, 'sort'))
end

mpd.playing = function()
	return system.singleResult(system.pipe(commands.status, 'grep -o "playing"')) == 'playing'
end

return mpd

